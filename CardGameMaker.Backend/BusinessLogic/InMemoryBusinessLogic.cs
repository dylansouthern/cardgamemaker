﻿namespace CardGameMaker.Backend.BusinessLogic
{
	public class InMemoryBusinessLogic : IBusinessLogic
	{
		public IGameLogic GameLogic { get; } = new InMemoryGames();
		public IUserLogic UserLogic { get; } = new InMemoryUsers();
	}
}