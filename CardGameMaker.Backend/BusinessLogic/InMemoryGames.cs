using System.Collections.Generic;
using CardGameMaker.Backend.Models;

namespace CardGameMaker.Backend.BusinessLogic
{
	public interface IGameLogic
	{
		List<Game> GetAllGames();
	}
	
	public class InMemoryGames : IGameLogic
	{
		private readonly List<Game> _games = new List<Game>();
		
		public List<Game> GetAllGames() {
			return _games;
		}

		public void CreateGame() {
			_games.Add(new Game());
		}
	}
}