using CardGameMaker.Backend.BusinessLogic;
using FluentAssertions;
using Xunit;

namespace CardGameMaker.Backend.Tests.BusinessLogic
{
	public class InMemoryGamesTests
	{
		[Fact]
		public void InitiallyThereAreNoGames() {
			var games = new InMemoryGames();
			games.GetAllGames().Should().BeEmpty();
		}

		[Fact]
		public void AfterCreatingAGame_ThereIsOneGame() {
			var games = new InMemoryGames();
			games.CreateGame();
			games.GetAllGames().Should().HaveCount(1);
		}
	}
}