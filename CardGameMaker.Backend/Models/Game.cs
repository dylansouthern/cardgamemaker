using System;

namespace CardGameMaker.Backend.Models
{
	public class Game
	{
		public Guid Id { get; set; } = Guid.NewGuid();

		public string Name { get; set; } = "My Game";
	}
}