using CardGameMaker.Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CardGameMaker.Controllers
{
    public class AuthorizationController : Controller
    {
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login([FromBody] User user)
        {
            return Ok(user);
        }
    }
}