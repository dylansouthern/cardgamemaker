namespace CardGameMaker.Backend.BusinessLogic {
	public interface IBusinessLogic
	{
		IGameLogic GameLogic { get; }
		IUserLogic UserLogic { get; }
	}
}