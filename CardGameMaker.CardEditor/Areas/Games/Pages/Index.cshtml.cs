using System.Collections.Generic;
using CardGameMaker.Backend.BusinessLogic;
using CardGameMaker.Backend.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace CardGameMaker.CardEditor.Games.Pages
{
	public class Index : PageModel
	{
		private readonly IBusinessLogic _logic;
		private IGameLogic GameLogic => _logic.GameLogic;
		
		public Index(IBusinessLogic logic) {
			_logic = logic;
		}
		
		public List<Game> Games { get; set; }
		
		public void OnGet() {
			Games = GameLogic.GetAllGames();
		}
	}
}