﻿using System;
using CardGameMaker.Backend.BusinessLogic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace CardGameMaker
{
    public class Startup : IStartup
    {
        IServiceProvider IStartup.ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                    .AddRazorPagesOptions(options => { options.AllowAreas = true; });

            var db = new InMemoryBusinessLogic();
            services.AddSingleton<IBusinessLogic>(db);
            return services.BuildServiceProvider();
        }

        public void Configure(IApplicationBuilder app)
        {
            var env = app.ApplicationServices.GetService<IHostingEnvironment>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStatusCodePagesWithReExecute("/errors/{0}");
            app.UseMvcWithDefaultRoute();
        }
    }
}